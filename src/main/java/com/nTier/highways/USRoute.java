package com.nTier.highways;

class USRoute implements HighwayInterface{
	public String calculateQuadrant(Highway highway)
	{	//true-even; false-odd
		int number=Integer.parseInt(this.substringHighway(highway.getHighway()));
	boolean numberType=this.getnumberType(number);
	if(numberType==false)
	{
	if((number>=1)&&(number<=49))
	{
		return "East";
	}
	else if((number>=51)&&(number<=99))
	{
		return "West";
	}	
	}
	else if(numberType==true)
	{
		if((number>=2)&&(number<=48))
		{
			return "North";
		}
		else if((number>=50)&&(number<=98))
		{
			return "South";
		}	
	}
	else
	{
		return null;
	}
	return null;
}
boolean getnumberType(int number)
{
	if(number%2==0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
public String substringHighway(String highway) {
	return highway.substring(3, highway.length());
}
}
