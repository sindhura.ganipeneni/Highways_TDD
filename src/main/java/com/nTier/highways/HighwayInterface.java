package com.nTier.highways;

import java.util.StringTokenizer;

interface HighwayInterface {
String calculateQuadrant(Highway highway);
String substringHighway(String highway);
}
