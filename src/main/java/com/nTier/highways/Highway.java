package com.nTier.highways;

import java.io.Serializable;

class Highway implements Serializable{
	private static final long serialVersionUID = 1L;
protected String highway;
protected String quadrant;

public Highway(String highway)
{
	this.highway=highway;
}
String getHighway() {
	return highway;
}
void setHighway(String highway) {
	this.highway = highway;
}
String getQuadrant() {
	return quadrant;
}
void setQuadrant(String quadrant) {
	this.quadrant = quadrant;
}

}
