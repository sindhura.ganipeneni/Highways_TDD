package com.nTier.highways;

class Interstate implements HighwayInterface  {
public String calculateQuadrant(Highway highway)
{	//true-even; false-odd
	int number=Integer.parseInt(this.substringHighway(highway.getHighway()));
	boolean numberType=this.getnumberType(number);
	if(numberType==false)
	{
	if((number>=1)&&(number<=49))
	{
		return "West";
	}
	else if((number>=51)&&(number<=99))
	{
		return "East";
	}	
	}
	else if(numberType==true)
	{
		if((number>=2)&&(number<=48))
		{
			return "South";
		}
		else if((number>=50)&&(number<=98))
		{
			return "North";
		}	
	}
	else
	{
		return null;
	}
	return null;
}
boolean getnumberType(int number)
{
	if(number%2==0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
public String substringHighway(String highway) {
	return highway.substring(2,highway.length());
}

}
