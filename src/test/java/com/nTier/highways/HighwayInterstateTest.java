package com.nTier.highways;

import static org.junit.Assert.*;

import org.junit.Test;

public class HighwayInterstateTest {

	@Test
	public void testInterstateWest() {
		Interstate i=new Interstate();
		Highway h=new Highway("I-5");
		assertEquals("West", i.calculateQuadrant(h));
	}
	
	@Test
	public void testInterstateEast() {
		Interstate i=new Interstate();
		Highway h=new Highway("I-99");
		assertEquals("East", i.calculateQuadrant(h));
	}

	@Test
	public void testInterstateSouth() {
		Interstate i=new Interstate();
		Highway h=new Highway("I-46");
		assertEquals("South", i.calculateQuadrant(h));
	}
	
	@Test
	public void testInterstateNorth() {
		Interstate i=new Interstate();
		Highway h=new Highway("I-98");
		assertEquals("North", i.calculateQuadrant(h));
	}
}
