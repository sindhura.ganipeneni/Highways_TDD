package com.nTier.highways;

import static org.junit.Assert.*;

import org.junit.Test;

public class HighwayUSRouteTest {

	@Test
	public void testUsRouteWest() {
		USRoute r=new USRoute();
		Highway h=new Highway("US-5");
		assertEquals("East", r.calculateQuadrant(h));
	}
	
	@Test
	public void testUsRouteEast() {
		USRoute r=new USRoute();
		Highway h=new Highway("US-99");
		assertEquals("West", r.calculateQuadrant(h));
	}

	@Test
	public void testUsRouteSouthth() {
		USRoute r=new USRoute();
		Highway h=new Highway("US-46");
		assertEquals("North", r.calculateQuadrant(h));
	}
	
	@Test
	public void testUsRouteNorth() {
		USRoute r=new USRoute();
		Highway h=new Highway("US-98");
		assertEquals("South", r.calculateQuadrant(h));
	}

}
